
# Intro

profil participants :

- qui a déjà été confronté à Git ?
- qui utilise déjà Git ? 
- avec quelle interface ?

passif ou actif :

- est-ce que vous souhaitez manipuler en même temps que j'explique ?
- qui a déjà Git sur son ordi ? VSCode ?

## Qu'est-ce que Git ?

## Pourquoi l'utiliser ?

## Pourquoi les gens en ont-ils si peur ?

> Ce que propose Git, c'est une gestion de version sous la forme d'un arbre. Et du coup la meilleure façon de ne rien comprendre à ce qui se passe c'est de lancer des commandes dans un terminal en essayant de se représenter un arbre dans sa tête. À la place on va plutôt utiliser une interface qui nous permet de visualiser cet arbre et d'interagir avec lui.

## Plan

# Partie 1: suivre l'évolution d'un projet

Je commence par là parce que c'est la partie la plus importante. Souvent les gens qui présentent Git commencent par la partie "travailler à plusieurs", parce que c'est l'usage final recherché par la plupart des utilisateurs. Mais du coup ça donne des gens qui savent envoyer du code en ligne, mais comprennent mal comment le système fonctionne sur leur ordi. Résultat à un moment ils font une fausse manip, ils cassent tout et ils paniquent.

Du coup on va d'abord se concentrer sur une utilisation en locale de Git. SI vous comprenez bien cette partie, le fait de travailler à plusieurs sur un projet vous paraîtra complètement logique et vous aurez jamais de problèmes que vous ne puissiez régler vous même.

## Les logiciels que nous allons utiliser

Je n'ai pas le temps de m'attarder sur l'installation. Si vous avez déjà ces deux logiciels installés vous pourrez manipuler en même temps que j'explique, sinon vous pouvez les installer pendant que j'avance. N'hésitez pas si vous avez des questions.

## Premiers pas

L'idée dans un premier temps c'est qu'on va créer un nouveau dépôt et jouer un peu avec. Ensuite dans un deuxième temps je vais vous presenter un dépôt un peu plus complexe, pour voir à quoi ça peut ressembler.

### 1.1. dépôt - changes - stage - commit

- initialiser un **dépôt**
- premier **commit**
  - choisir le contenu: **stage**
  - écrire un **message de commit**

autre points :

- stage / unstage / revert selected range
- infos contenues dans le commit
- bonnes pratiques commit

### 1.2. branch - checkout

- créer une nouvelle **branche**
- changer de branche: **checkout**

_itérer sur les étapes, en montrant les diff dans l'arbre_

- montrer le changement effectif de contenu du dossier


autres points :

- stash (apply / pop / drop)

### bilan provisoire

À ce stade, on a tous les outils de base pour gérer notre dépôt :

- **dépôt**   : utiliser Git dans un dossier
- **changes** : suivre ses changements
- **stage**   : choisir le contenu du prochain commit
- **commit**  : enregistrer ses changements
- **branch**  : organiser ses changements
- **checkout**: changer de branche
- **stash**   : mettre ses changements de côté

### 1.3. drop - cherry pick - revert - merge - rebase - reset 

Et là normalement vous avez tous les outils pour gérer votre dépôt Git de manière classique. C'est là que pour les gens qui utilisent Git à la ligne de commande les problèmes commencent. En fait à une commande prêt c'est là qu'ils s'arrêtent parce que faire des choses plus compliquées c'est la meilleure façon de tout casser.

Ce que je vais vous présenter là dites-vous bien que 80 ou 90 % des étudiants de l'Ensimag en sont complètement incapables, simplement parce que à la ligne de commande c'est un enfer à utiliser.

Mais nous avec notre interface graphique on va pouvoir faire ces choses là de manière beaucoup plus intuitive, et donc on va pas se limiter à une utilisation basique mais aller au bout de l'idée.

Alors du coup la question que je vais poser, surtout à ceux qui utilisent déjà Git, c'est qu'est-ce que vous aimeriez pouvoir faire dans ce dépôt pour vous sentir vraiment libres ?

- annuler un commit
- récupérer le contenu d'un commit dans une autre branche
- renommer un commit
- fusionner des commits
- revenir à un ancien commit
- modifier l'ordre des commits
- fusionner des branches

- **drop**          : supprime un commit
- **cherry pick**   : ajoute le contenu d'un commit à la tête d'une branche
- **revert**        : crée un nouveau commit inversant les modifications d'un autre
- **rebase**        : change la racine d'une branche
- **reset**         : déplace la tête d'une branche
- **merge**         : fusionne une branche dans une autre
- **squash**        : crée un nouveau commit dont le contenu est l'équivalent d'un merge

## Cas d'utilisation

Maintenant qu'on a tout un panel d'outils pour gérer un dépôt, on va passer sur un exemple plus concret d'utilisation de Git, avec deux idées :

- montrer la façon dont on utilise ce que permet Git pour se simplifier la vie dans le monde du dev
- proposer quelques cas d'usage permettant d'utiliser les outils précédents

_ouvrir un dossier vide dans VSCode et récupérer un dépôt (pas depuis un git clone, on veut que des branches locales)_

### 2.1. Observation

- messages de commits
- branches main/dev

### 2.2. Cas d'usage

**facile**

- annuler l'ajout du plugin manager dans la branche dev

**difficile**

- renommer le commit "

## Détails en vrac

- le cas des dossiers et .gitkeep
- ignorer des fichiers avec .gitignore
- les tags

# Partie 2: travailler à plusieurs

Maintenant qu'on a vu comment utiliser un depot en local on va s'interesser a la partie "travailler a plusieurs". 

## 1. Main diagram

En fait c'est vraiment pas tres complique. On a plusieurs client et un serveur. Tous y compris le serveur sont de sdezpots git completement normaux. Simplement on va creer de nouvelles branches qui vous representer la version serveur de nos branches locales.

## 2. remote - push - pull - fetch

La seule chose importante a comprendre c'est le fait qu'on a une branche sur notre depot qui represente la branche sur le serveur. Mais la synchronisation est pas automatique. En fait Git ne fait aucune requete vers le serveur sans qu'on lui demande explicitement.

Donc pour mettre a jour notre version des branches du serveur on a une commande :
**fetch**.

Ensuite ce qu'on veut pouvoir faire c'est synchroniser notre branche locale avec la branche du serveur. Pour ca on a deux commandes: **push** et **pull*

Par aileurs je vais vous donner une commande que vous ne pouvez pas faire depuis l'interface mais qui est tres utile : git push -f. Elle vous permet de forcer une branche remote a copier une branche locale.

# Ouverture

Git c'est un outil super puissant mais surtout adopte massivement

# Conlusion

J'espere ne pas vous avoir complemtenent perdu, et si possible vous avoir donne des billes pour aborder d'une facon apaisee.

A propos de l'interface graphique, il faut garder en tete que toutes les notions que je vous ai presente correspondent a des commandes de l'utilitaire git. Mais l'interface permet d'appeler directement les commandes avec les options qu'on utilise le plus.

Mais si jamais vous avez un jour besoin de faire quelque chose d'un peu inhabituel, par exemple changer l'adresse du serveur, d'ajouter un deuxieme serveur, ce genre de choses, internet vous donnera tres facilement les commandes a utiliser.

# Ressources

l'instant promo: je travaille avec l'equipe COSYS jusqu'en fevrier, pour la suite on verra bien. Si vous avez des besoins n'hesitez pas a me contacter !

- accompagnement des developpements et formation aux outils et bonnes pratiques
- administration systeme
- dev (dans une moindre mesure)

## Détails en vrac

- l'intégration continue
