---
title: |  
  ![](includes/git.png){width=4em}  
  An (hopefully) understandable  
  approach of Git
author: Rémy Le Calloch
date: 16/12/2022
---

# Intro

Let's start with some questions !

## What is Git ?

| | | |
|-|-|-|
| |![](includes/git.png){height=20%}| |

A distributed version control system (logiciel de gestion de version décentralisé)

- keeping track of the evolutions of a project
- working asynchronously with others 

## Why using Git ?

Working with others

- a lot of projects use it (ex: Github)
- it is a standard
- its goal is to help people working on the same project

Keeping your work history

- it is the main concept of Git

Becoming more rigorous

- Git forces you to explain what you do and keep your work organized 

## Why are people so afraid of Git ?

> Less than 50% of Ensimag students are comfortable with git.

People fears:

- losing too much time trying to understand it
- breaking the repository and having to recreate a new one
- losing their work

Why these problems ?

- learning it the wrong way
- **using it from command line**

## Summary

**Part 1: tracking the evolution of a project**

1. First steps
   1. saving your changes
   2. branching
   3. advanced commands
2. A realistic example
   1. observations
   2. usecase

**Part 2: working with others**

1. Main diagram
   1. basic
   2. advanced
   3. advanced commands
2. Commands

**Conclusion / Doorways / Ressources**

## Softwares we are going tu use

![](includes/git.png){height=1em} Git: [git-scm.com](https://git-scm.com/downloads)

![](includes/vscode.png){height=1em} Visual Studio Code: [code.visualstudio.com](https://code.visualstudio.com/download)

- ![](includes/git_graph.png){height=1em} Git Graph extension

# Part 1: tracking the evolution of a project

1. First steps
   1. saving your changes
   2. branching
   3. advanced commands
2. A realistic example
   1. observations
   2. usecase

## Part 1 | 1.1. repository - changes - stage - commit - undo last

![](includes/111.png)

|                      |                                      |
|----------------------|--------------------------------------|
| **repository**       | folder with a hidden git folder      |
| **changes**          | modifications made since last commit |
| **stage**            | changes to include in next commit    |
| **commit**           | saved state of the folder            |
| **undo last commit** | speaks for itself                    |

## Part 1 | 1.2. branch - checkout - stash

![](includes/112.png)

|                 |                                 |
|-----------------|---------------------------------|
| **branch**      | folder with a hidden git folder |
| **checkout**    | switch to another branch        |
| **stash**       | put aside uncommited changes    |
| **apply stash** | restore stash content           |
| **drop stash**  | delete a stash                  |
| **pop stash**   | apply and delete a stash        |

## Part 1 | Recap

**Tracking changes**

|                      |                                      |
|----------------------|--------------------------------------|
| **repository**       | folder with a hidden git folder      |
| **changes**          | modifications made since last commit |
| **stage**            | changes to include in next commit    |
| **commit**           | saved state of the folder            |
| **undo last commit** | speaks for itself                    |

**Branching**

|                 |                                 |
|-----------------|---------------------------------|
| **branch**      | folder with a hidden git folder |
| **checkout**    | switch to another branch        |
| **stash**       | put aside uncommited changes    |
| **apply stash** | restore stash content           |
| **drop stash**  | delete a stash                  |
| **pop stash**   | apply and delete a stash        |

## Part 1 | 1.3. drop - cherry pick - revert - rebase - reset - merge

|                 |                                                       |
|-----------------|-------------------------------------------------------|
| **drop**        | delete the changes introduced by a commit             |
| **cherry pick** | get the changes introduced by a commit                |
| **revert**      | create a new commit doing the opposite of another one |
| **rebase**      | root a branch to a commit from another one            |
| **reset**       | put the head of a branch at a certain commit          |
| **merge**       | "smart" merge of a branch into another one            |

## Part 1 | 2. A realistic example

![](includes/12.png)

## Part 1 | 2.1. Observation

![](includes/121.png)

- commit messages
- main/dev branches

## Part 1 | 2.2. Usecase

**easy**

- rename plugin-manager-dev to pm-dev
- create a new branch at the 0.2.0 version

**hard**

- compact last 2 commits of dev into one.

## Part 1 | Other notes

![](includes/13.png)

- empty folders: .gitkeep
- ignore files: .gitignore
- tags

# Part 2: working with others

1. Main diagram
   1. basic
   2. advanced
   3. advanced commands
2. Commands

## Part 2 | 1.1. Main diagram: basic

![](includes/211.png)

## Part 2 | 1.2 Main diagram: advanced

![](includes/212.png)

## Part 2 | 2.1. local - remote - push - pull - fetch

![](includes/221.png){height=50%}

|            |                                                   |
|------------|---------------------------------------------------|
| **remote** | repository on a server                            |
| **fetch**  | sync remote branches with server                  |
| **pull**   | merge remote branch into local                    |
| **push**   | merge local branch into remote and send to server |

Bonus: `git push -f` forces remote to sync wit local

## Part 2 | 2.1. local - remote - push - pull - fetch

![](includes/222.png)

|            |                                                   |
|------------|---------------------------------------------------|
| **remote** | repository on a server                            |
| **fetch**  | sync remote branches with server                  |
| **pull**   | merge remote branch into local                    |
| **push**   | merge local branch into remote and send to server |

Bonus: `git push -f` forces remote to sync wit local

# Conclusion

Git is not that mysterious...

... if you use it from a GUI ! 

I hope this presentation gaves you a better view of it.

# Doorway

**Gitlab/Github**; additional functionalities

**Gricad Gitlab**: host repositories in G-SCOP

**Markdown**: a readable Latex for everyday life

**CI/CD**: test and deploy your code automatically

# Ressources

contact: [remy@lecalloch.net](mailto:remy@lecalloch.net)

repos of this presentation: [framagit.org/pan6ora/git-tutorial](https://framagit.org/pan6ora/git-tutorial)

- slides
- script and personal notes (fr)
- cheatsheet of Git commands


