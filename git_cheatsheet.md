First setup
```
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
git config --global init.defaultBranch main
```

Creating a new repos
```
git init
git remote add origin git@xxx:xxx+/xxx.git
```

Getting an existing repos
```
git clone git@xxx:xxx+/xxx.git
```

Managing remotes
```
git remote -v                           list all remotes
git remote add -f --tags <name> <URL>   add a remote repos
git remote remove <name>                delete a remote
```

Fetching
```
git fetch --all -a      fetch from all remotes
```

Commit and push
```
git status              show what can be commit

git add <filename>      add a file to next commit
git add .               add all changes to next commit

git commit -m "This is a commit message"    commit

git push                send changes tu repos
```

# Git tools

**Tracking changes**

|                      |                                      |
|----------------------|--------------------------------------|
| **repository**       | folder with a hidden git folder      |
| **changes**          | modifications made since last commit |
| **stage**            | changes to include in next commit    |
| **commit**           | saved state of the folder            |
| **undo last commit** | speaks for itself                    |

**Branching**

|                 |                                 |
|-----------------|---------------------------------|
| **branch**      | folder with a hidden git folder |
| **checkout**    | switch to another branch        |
| **stash**       | put aside uncommited changes    |
| **apply stash** | restore stash content           |
| **drop stash**  | delete a stash                  |
| **pop stash**   | apply and delete a stash        |

**Advanced**

|                 |                                                       |
|-----------------|-------------------------------------------------------|
| **drop**        | delete the changes introduced by a commit             |
| **cherry pick** | get the changes introduced by a commit                |
| **revert**      | create a new commit doing the opposite of another one |
| **rebase**      | root a branch to a commit from another one            |
| **reset**       | put the head of a branch at a certain commit          |
| **merge**       | "smart" merge of a branch into another one            |

**Remote**

|            |                                                   |
|------------|---------------------------------------------------|
| **remote** | repository on a server                            |
| **fetch**  | sync remote branches with server                  |
| **pull**   | merge remote branch into local                    |
| **push**   | merge local branch into remote and send to server |